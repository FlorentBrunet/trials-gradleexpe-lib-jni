package com.ubleam.mylibrary;

public class MyTrialLib {

    public static int dummy() {
        return 789;
    }

    // Should always return 123
    public static native int dummyNative();

    static {
        System.loadLibrary("dummylib");
    }

}
